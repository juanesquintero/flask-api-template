import pydash as _
from flask_restful import Resource
from api.schemas.schema import validate_example_schema
from api.shared.utils import response, required

DATA = [
    {'id': 1, 'foo': 'stuff_1', 'bar': 'stuff_2'},
    {'id': 2, 'foo': 'stuff_3', 'bar': 'stuff_4'},
    {'id': 3, 'foo': 'stuff_5', 'bar': 'stuff_6'}
]

class Controller(Resource):

    def get(self, id_item=None):
        if id_item is None:
            return response(status=200, data=DATA)

        item = self.get_item(id_item)
        if item:
            return response(status=200, data=item)
        return response(status=404, message='Not found item')

    # pylint: disable=R0201, W0613
    @required('id_item', False)
    def post(self, id_item=None):
        validate_example_schema({})
        return response(status=201, message='created')

    @required('id_item')
    def put(self, id_item=None):
        validate_example_schema({})
        item = self.get_item(id_item)
        if item:
            return response(status=202, message='updated')
        return response(
            status=404,
            message='Not updated, not found item'
        )

    # pylint: disable=R0201
    @required('id_item')
    def patch(self, id_item=None):
        validate_example_schema({id:id_item})
        return response(status=202, message='patched')

    @required('id_item')
    def delete(self, id_item=None):
        validate_example_schema({})
        item = self.get_item(id_item)
        if item:
            return response(status=202, message='deleted')
        return response(
            status=404,
            message='Not deleted, not found item'
        )

    @staticmethod
    def get_item(id_item):
        return _.filter_(DATA, {'id': id_item})
