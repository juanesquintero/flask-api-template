from functools import wraps
from flask import abort

def response(**kwargs):
    status = kwargs.get('status', 200)
    kwargs['status'] = status
    if not kwargs.get('error'):
        kwargs['error'] = str(status)[0] != '2'
    return  kwargs, status

def required(param, expected=True):
    def decorate(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            if expected and kwargs.get(param) is None:
                return abort(405)
            if not expected and kwargs.get(param) is not None:
                return abort(405)
            return func(*args, **kwargs)
        return wrapper
    return decorate
