HTTP_ERRORS = {
    'BadRequest': {
        'status': 400,
        'error': True,
        'message':'Bad Request'
    },
    'Unauthorized': {
        'status': 401,
        'error': True,
        'message': 'Invalid credentials'
    },
    'Forbidden':{
        'status': 403,
        'error': True,
        'message':'Forbidden'
    },
    'NotFound': {
        'status': 404,
        'error': True,
        'message':'Endpoint Not Found'
    },
    'MethodNotAllowed': {
        'status': 405,
        'error': True,
        'message':'Method Not Allowed'
    },
    'RequestTimeout': {
        'status': 408,
        'error': True,
        'message':'Request Timeout'
    },
    'InternalServerError': {
        'status': 500,
        'error': True,
        'message':'Internal Server Error'
    },
    'SchemaValidationError': {
        'status': 400,
        'error': True,
        'message': 'Request is missing required fields'
    }
}
