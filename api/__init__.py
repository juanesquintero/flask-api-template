from flask import Flask, jsonify, request
from flask_cors import CORS
from flask_restful import Api, Resource, abort
from api.routes.controller import Controller
from api.shared.utils import response
from api.shared.constants import HTTP_ERRORS

def create_api(env='dev'):
    # Inicializar Aplicacion de Flask
    app = Flask(__name__)
    CORS(app)

    with app.app_context():
        # Configuracion base de app
        config(app, env)

        # Manejador/Captura de errores
        error_handlers(app)

        # Registrar rutas con vistas del api
        routes(app)

    return app


def config(api, env):
    # Definir variables de configuracion (desde config.py)
    environments = {
        'dev': 'config.DevConfig',
        'test': 'config.TestConfig',
        'prod': 'config.ProdConfig'
    }
    env_config = environments.get(env, 'config.Config')
    api.config.from_object(env_config)


def routes(app):
    api = Api(
        app=app,
        default_mediatype='application/json',
        errors=HTTP_ERRORS,
        catch_all_404s=True
    )
    root_path = app.config.get('ROOT_PATH', '/')
    # pylint: disable=unused-variable,R0201,R0903
    class Index(Resource):
        res = response(api='Flask API Template')
        def get(self):
            return self.res
        def post(self):
            return self.res

    # Register routes
    api.add_resource(Index, root_path, f'{root_path}/')
    api.prefix = root_path
    api.add_resource(Controller, '/controller', '/controller/', '/controller/<int:id_item>')


def error_handlers(app):
    error_logger = app.config.get('ERROR_LOGGER', None)

    # pylint: disable=unused-variable,unused-argument
    @app.errorhandler(404)
    def not_found(error):
        return response(**HTTP_ERRORS['NotFound'])

    @app.errorhandler(405)
    def method_not_allow(error):
        print(1)
        return response(**HTTP_ERRORS['MethodNotAllowed'])

    @app.errorhandler(500)
    def internal_server_error(error):
        error_logger.error(error)
        return response(**HTTP_ERRORS['InternalServerError'])

    @app.errorhandler(Exception)
    def exception(error):
        error_logger.error(f'EXCEPTION: {error}')
        return response(**HTTP_ERRORS['InternalServerError'])
