from jsonschema import Draft7Validator, draft7_format_checker

example_schema = {
    'type': 'object',
    'properties': {
        'filed':  {'type': 'string', 'maxLength': 50}
    },
    'required': [
        'field'
    ],
    'additionalProperties': False
}


def validate_example_schema(json):
    return Draft7Validator(example_schema, format_checker=draft7_format_checker).is_valid(json)
