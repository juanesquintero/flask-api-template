'''
The main config file for Superset

All configuration in this file can be overridden by providing a superset_config
in your PYTHONPATH as there is a ``from superset_config import *``
at the end of this file.
'''
# pylint: disable=R0903
import os
import logging
import datetime
from dotenv import load_dotenv

load_dotenv()
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
LOG_FORMAT = '%(asctime)s:%(levelname)s:%(name)s:%(message)s'
LOG_DIR = BASE_DIR + '/logs'

class Config():
    # Basic config vars
    APP_NAME = 'Flask API'
    APP_ICON = '/static/img/logo.png'
    CURRENT_YEAR = datetime.datetime.now().year
    JSON_SORT_KEYS = False
    ROOT_PATH = '/api/v1'

    # Reloadn on Jinja templates change
    TEMPLATES_AUTO_RELOAD = True
    DEBUG_TB_ENABLED = False

    # Session Config
    SECRET_KEY = os.environ.get('ABCREP_SECRET_KEY')
    SESSION_PERMANENT = True
    SESSION_TYPE = 'filesystem'
    PERMANENT_SESSION_LIFETIME = datetime.timedelta(minutes=60)

    # Folders to save uploads
    UPLOAD_FOLDER = BASE_DIR + '/app/static/uploads/'

    # ERROR LOGS
    file_handler = logging.FileHandler(f'{LOG_DIR}/ERRORS.log')
    file_handler.setFormatter(logging.Formatter(LOG_FORMAT))
    ERROR_LOGGER = logging.getLogger('error_logger')
    ERROR_LOGGER.setLevel(logging.ERROR)
    ERROR_LOGGER.addHandler(file_handler)

    # OAuth2 config
    OAUTH_CLIENT_ID = os.environ.get('OAUTH_CLIENT_ID')
    OAUTH_CLIENT_SECRET = os.environ.get('OAUTH_CLIENT_SECRET')
    ACCESS_TOKEN_URL = 'oauth/token'

class DevConfig(Config):
    TESTING = False
    PRESERVE_CONTEXT_ON_EXCEPTION = True
    ENV = 'development'

class TestConfig(Config):
    TESTING = True
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    ENV = 'testing'

class ProdConfig(Config):
    TESTING = False
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    ENV = 'production'

    # GENERAL LOGS (ALL)
    logging.getLogger().setLevel(logging.DEBUG)
    logging.basicConfig(filename=f'{LOG_DIR}/GENERALS.log', level=logging.DEBUG, format=LOG_FORMAT)
