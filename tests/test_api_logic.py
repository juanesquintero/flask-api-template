# pylint: disable=redefined-outer-name,invalid-name,unused-argument,unused-variable
# pylint: disable=R0201,E1101, W0221
from unittest import TestCase, mock
from flask_unittest import AppTestCase
import api as APIModule

class TestAPILogic(AppTestCase):
    '''
    TestAPILogic:\n
    This class test the business logic of the application
    is the actual verification of the functionality and flow.
    '''

    def create_app(self):
        return APIModule.create_api('test')

    def setUp(self, api):
        # Perform set up before each test, using api
        pass

    def tearDown(self, api):
        # Perform tear down after each test, using api and client
        pass

class TestCreationLogic(TestCase):
    '''
    TestAPIController:\n
    This class test the business logic of and specific controller.
    '''

    @staticmethod
    def assertion_call(mock):
        APIModule.create_api('test')
        mock.assert_called()

    @mock.patch.object(APIModule, 'config')
    def test_config_to_have_been_called(self, mock):
        self.assertion_call(mock)

    @mock.patch.object(APIModule, 'routes')
    def test_routes_to_have_been_called(self, mock):
        self.assertion_call(mock)

    @mock.patch.object(APIModule, 'error_handlers')
    def test_error_handlers_to_have_been_called(self, mock):
        self.assertion_call(mock)


class TestControllerLogic(TestAPILogic):
    '''
    TestAPIController:\n
    This class test the business logic of and specific controller.
    '''

    def test_controller_logic(self, api):
        self.assertEqual(True, 1)
